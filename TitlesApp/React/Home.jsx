﻿var _titles = [];

var getTitles = function (name) {
       return axios.get('/api/titles/' + name)         
}

//Home Top Level Component
var Home = React.createClass({
    getInitialState: function () {
        return {titles : _titles};
    },
    render: function () {
        var data = _titles;
        return (
            <div className="Home">
                <h1>
                    Turner Titles
                </h1>
                <Filters />
                <TitleList data={data}/>
            </div>


        );
    }
});

//Filter Component
var Filters = React.createClass({
    getInitialState: function () {
        return this.state = { value: "" };
    },
    render: function () {
        var that = this;
        return (
            <div className="filters">
                <div>
                    <div className="input-controls">
                        <h4>Search by Title:</h4>
                        <input type="text" value={that.state.value} onChange={that.handleChange} />
                        <button className="btn btn-info" filter={that.state.value} onClick={() => this.filterTitles(that.state.value)}><span className="glyphicon glyphicon-edit"></span></button>
                        <button className="btn btn-info" onClick={that.clearFilters}><span className="glyphicon glyphicon-remove"></span></button>
                    </div>
                </div >
            </div >
        );
    },
    handleChange: function (event) {
        this.setState({ value: event.target.value });
    },
    filterTitles: function (props) {
        getTitles(props).then(function (response) {
            _titles = response.data;
            ReactDOM.render(
                <Home />,
                document.getElementById('mount-point')
            );
        })
    },
    clearFilters: function() {
        getTitles("").then(function (response) {
            _titles = response.data;
            ReactDOM.render(
                <Home />,
                document.getElementById('mount-point')
            );
        });
    }
});

function filterTitlesS(nameFilter) {
    getTitles().then(function (response) {
        _titles = response.data;
        ReactDOM.render(
            <Home />,
            document.getElementById('mount-point')
        );
    })
}


//TitleList Component
var Accordion = ReactSanfona.Accordion;
var AccordionItem = ReactSanfona.AccordionItem;
var TitleList = React.createClass({
    toString: function (y, pN, pV) {
        var result = "";
        if (y[pN]) {
            y[pN].forEach(function (z) {
                if (z['GenreId']) {
                    result = result + "," + z[pV] + "(" + z['GenreId'] + ")";
                }
            });
        }
        if (result) { result = result.slice(1, result.length) }
        return result;
    },
    render: function () {
        return (
            <Accordion class="titlelist">
                {this.props.data.map((_data, index) => {
                    return (
                        <AccordionItem title={`${_data.TitleName}`} slug={_data.TitleName.substring(1, 5) + index} key={_data.TitleName.substring(1, 5) + index}>
                            <div>{`Genres: ${this.toString(_data,"TitleGenres","Genre")}`}</div>
                            <div>{`Release Year: ${_data.ReleaseYear}`}</div>
                        </AccordionItem>
                    );
                }, this)}
            </Accordion>
        );
    }
});



(function () {
    getTitles("").then(function (response) {
        _titles = response.data;
        ReactDOM.render(
            <Home />,
            document.getElementById('mount-point')
        );
    })
})()
