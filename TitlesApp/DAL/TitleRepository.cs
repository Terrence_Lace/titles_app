﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TitlesApp.Models;
using TitlesApp.Models.ViewModels;
using System.Data.Entity;

namespace TitlesApp.DAL
{
    public class TitleRepository : ITitleRepository
    {
        public IList<Title> GetTitleByName(string name)
        {
            List<Title> result;
            using (var db = new TitlesDb())
            {
                db.Configuration.LazyLoadingEnabled = false;
                //result = db.Titles.Where(t => t.TitleName.Contains(name)).ToList<Title>();
                result = db.Titles.Where(t => t.TitleName.Contains(name)).Include(e => e.TitleGenres).ToList<Title>();
            }

            return result;
        }

        public IList<Title> GetTitles()
        {
            List<Title> result;
            using (var db = new TitlesDb())
            {
                db.Configuration.LazyLoadingEnabled = false;
                //result = db.Titles.ToList<Title>();
                result = db.Titles.Include(e => e.TitleGenres).ToList();
            }
            return result;
        }
    }
}