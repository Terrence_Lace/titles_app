﻿(function (global) {
    System.config({
        paths: {
            // paths serve as alias
            'npm:': 'node_modules/'
        },
        // map tells the System loader where to look for things
        map: {
            // other libraries
            'rxjs': 'npm:rxjs',
            "react-sanfora": "node_modules/react-sanfora"
        },
        // packages tells the System loader how to load when no filename and/or no extension
        packages: {
           rxjs: {
                defaultExtension: 'js'
            },
           "react-sanfora": { "main": "react-sanfora.js", "defaultExtension": "js" }
        }
    });
})(this);