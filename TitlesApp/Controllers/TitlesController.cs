﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TitlesApp.Models;

namespace TitlesApp.Controllers
{
    public class TitlesController : ApiController
    {
        private ITitleRepository titlesrepository;

        public TitlesController(ITitleRepository titlesrepository)
        {
            this.titlesrepository = titlesrepository;
        }

        // GET api/Titles
        [HttpGet]
        [Route("api/Titles")]
        public IList<Title> Get()
        {
            return titlesrepository.GetTitles();
        }

        [HttpGet]
        [Route("api/Titles/{name}")]
        // GET api/Titles/All
        public IList<Title> Get(string name)
        {
            return titlesrepository.GetTitleByName(name);
        }

    }
}