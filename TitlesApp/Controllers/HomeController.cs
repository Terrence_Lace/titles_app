﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TitlesApp.Models;
using TitlesApp.Models.ViewModels;

namespace TitlesApp.Controllers
{
    public class HomeController : Controller
    {
        private ITitleRepository titlesrepository;

        public HomeController(ITitleRepository titlesrepository)
        {
            this.titlesrepository = titlesrepository;
        }

        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";
            var model = new HomeViewModel();

            return View(model);
        }

        public ActionResult GetTitles()
        {
            return Json(titlesrepository.GetTitles());
        }

        public ActionResult GetTitlesByName(string name)
        {
            return Json(titlesrepository.GetTitleByName(name));
        }

        public ActionResult GetDetails(string id)
        {
            return Json("");
        }
    }
}
