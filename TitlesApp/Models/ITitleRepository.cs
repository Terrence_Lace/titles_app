﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TitlesApp.Models
{
    public interface ITitleRepository
    {

        IList<Title> GetTitleByName(string name);

        IList<Title> GetTitles();
    }
}