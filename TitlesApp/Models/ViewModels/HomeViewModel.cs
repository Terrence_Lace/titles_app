﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TitlesApp.Models.ViewModels
{
    public class HomeViewModel
    { 
        public IList<Title> titles { get; set; }
    }
}