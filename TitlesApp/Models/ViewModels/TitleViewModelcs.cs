﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TitlesApp.Models.ViewModels
{
    public class TitleViewModel
    {
        public TitleViewModel(Title title, int? releaseYear, IList<Genre> genres)
        {
            this.Title = title;
            this.ReleaseYear = releaseYear;
            this.Genres = genres;
        }

        public Title Title { get; set; }

        public int? ReleaseYear { get; set; }

        public IList<Genre> Genres { get; set; }
    }

    public class GenreViewModel
    {
        public string name { get; set; }

        public int id { get; set; }
    }
}